var assert = require("assert");
var myMath = require('../');

describe('Math', function(){
	describe('adition', function(){
		it('should return a value idependent of order of params', function(){
			assert.equal(11, myMath(7,4).add());
			assert.equal(11, myMath(4,7).add());
		});
	});
	describe('minus', function(){
		it('should minus two numbers', function(){
			assert.equal(2, myMath(4,2).minus());
		});
		it('should\'t have negative returns', function(){
			assert.equal(11, myMath(0,11).minus());
			assert.equal(5, myMath(1,6).minus());
		});
	});
	describe('times', function(){
		it('should miltiply two numbers', function(){
			assert.equal(8, myMath(4,2).times());
			assert.equal(0, myMath(4,0).times());
			assert.equal(4, myMath(4,1.2).times());
			assert.equal(12, myMath(4,3.2).times());
		});
	});
	describe('divide', function(){
		it('should divide two numbers', function(){
			assert.equal(2, myMath(4,2).division());
			assert.equal(12, myMath(24,2).division());
		});
	});
});