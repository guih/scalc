module.exports = function(a, b) {
	
	var a = parseInt(a)
	, b = parseInt(b);


	return {
		add: function(){
			return a + b;
		},
		minus: function(){
			return ( a > b ) ? a - b : -(a - b);
		},
		times: function(){
			return a * b ;
		},
		division: function(){
			return a / b;
		}
	};
}